CXX			= g++
CXXFLAGS    = -Wall -g
# Windows
LIBS		= -lglfw -lopengl32 -lglu32
SHELL       = C:/Windows/System32/cmd.exe
# Linux
# LIBS		= -lglfw -libglu -libgl
INCLUDES	= -I./include

CFLAGS		= $(CXXFLAGS) $(INCLUDES)
LFLAGS		= $(CXXFLAGS)

TARGET		= bin/main

OBJS		= objs/main.o objs/GLWindow.o objs/Vector.o objs/Color.o objs/Ray.o \
				objs/Scene.o objs/Camera.o objs/Input.o \
				objs/Sphere.o objs/Plane.o objs/Triangle.o \
				objs/BoundingBox.o

MKDIR_P		= mkdir -p
.PHONY		= directories
OUT_DIR		= bin objs
directories	= $(OUT_DIR)

all:		directories $(TARGET)

directories:	$(OUT_DIR)

$(OUT_DIR):
		$(MKDIR_P) $(OUT_DIR)

$(TARGET): $(OBJS)
			$(CXX) $(LFLAGS) $(OBJS) $(LIBS) -o $(TARGET)

objs/GLWindow.o: src/GLWindow.cpp
			$(CXX) $(CFLAGS) -c src/GLWindow.cpp -o objs/GLWindow.o

objs/main.o: src/main.cpp
			$(CXX) $(CFLAGS) -c src/main.cpp -o objs/main.o

objs/Vector.o: src/Vector.cpp
			$(CXX) $(CFLAGS) -c src/Vector.cpp -o objs/Vector.o

objs/Color.o: src/Color.cpp
			$(CXX) $(CFLAGS) -c src/Color.cpp -o objs/Color.o
			
objs/Ray.o: src/Ray.cpp
			$(CXX) $(CFLAGS) -c src/Ray.cpp -o objs/Ray.o

objs/Scene.o: src/Scene.cpp
			$(CXX) $(CFLAGS) -c src/Scene.cpp -o objs/Scene.o

objs/Camera.o: src/Camera.cpp
			$(CXX) $(CFLAGS) -c src/Camera.cpp -o objs/Camera.o

objs/Input.o: src/Input.cpp
			$(CXX) $(CFLAGS) -c src/Input.cpp -o objs/Input.o

objs/Sphere.o: src/Sphere.cpp
			$(CXX) $(CFLAGS) -c src/Sphere.cpp -o objs/Sphere.o

objs/Plane.o: src/Plane.cpp
			$(CXX) $(CFLAGS) -c src/Plane.cpp -o objs/Plane.o

objs/Triangle.o: src/Triangle.cpp
			$(CXX) $(CFLAGS) -c src/Triangle.cpp -o objs/Triangle.o

objs/BoundingBox.o: src/BoundingBox.cpp
			$(CXX) $(CFLAGS) -c src/BoundingBox.cpp -o objs/BoundingBox.o

TEST_CFLAGS = $(CFLAGS)
TEST_CFLAGS += -I test/include
GTEST_OBJS = test/objs/gtest-all.o
TEST_OBJS = test/objs/test.o test/objs/VectorTest.o test/objs/ColorTest.o \
			test/objs/SceneTest.o test/objs/InputTest.o \
			test/objs/TriangleTest.o test/objs/SphereTest.o test/objs/PlaneTest.o \
			test/objs/BoundingBoxTest.o
TESTED_OBJS = objs/Vector.o objs/Color.o objs/Ray.o \
				objs/Scene.o objs/Camera.o objs/Input.o \
				objs/Sphere.o objs/Plane.o objs/Triangle.o \
				objs/BoundingBox.o
ALL_TEST_OBJS = $(TESTED_OBJS) $(GTEST_OBJS) $(TEST_OBJS)
TEST_TARGET = bin/test

test: $(TEST_TARGET)

bin/test: $(ALL_TEST_OBJS)
			$(CXX) $(LFLAGS) $(ALL_TEST_OBJS) -o $(TEST_TARGET)
 
test/objs/test.o: test/RunTests.cpp
			$(CXX) $(TEST_CFLAGS) -c test/RunTests.cpp -o test/objs/test.o

test/objs/VectorTest.o: test/VectorTest.cpp
			$(CXX) $(TEST_CFLAGS)  -c test/VectorTest.cpp -o test/objs/VectorTest.o
			
test/objs/ColorTest.o: test/ColorTest.cpp
			$(CXX) $(TEST_CFLAGS)  -c test/ColorTest.cpp -o test/objs/ColorTest.o
			
test/objs/SceneTest.o: test/SceneTest.cpp
			$(CXX) $(TEST_CFLAGS) -c test/SceneTest.cpp -o test/objs/SceneTest.o

test/objs/InputTest.o: test/InputTest.cpp
			$(CXX) $(TEST_CFLAGS) -c test/InputTest.cpp -o test/objs/InputTest.o

test/objs/TriangleTest.o: test/TriangleTest.cpp
			$(CXX) $(TEST_CFLAGS) -c test/TriangleTest.cpp -o test/objs/TriangleTest.o

test/objs/SphereTest.o: test/SphereTest.cpp
			$(CXX) $(TEST_CFLAGS) -c test/SphereTest.cpp -o test/objs/SphereTest.o

test/objs/PlaneTest.o: test/PlaneTest.cpp
			$(CXX) $(TEST_CFLAGS) -c test/PlaneTest.cpp -o test/objs/PlaneTest.o

test/objs/BoundingBoxTest.o: test/BoundingBoxTest.cpp
			$(CXX) $(TEST_CFLAGS) -c test/BoundingBoxTest.cpp -o test/objs/BoundingBoxTest.o


clean:
			rm -f $(OBJS)
			rm -f $(TARGET)
cleantest:
			rm -f $(TEST_TARGET)
			rm -f $(TEST_OBJS);
