#ifndef CAMERA_H
#define CAMERA_H
#include "Vector.h"
#include "Ray.h"
#include <cmath>
#define DEGTORAD(X) (X*M_PI/180.0) 

//! A Camera class.
/*!
    The Camera class provides an object for describing the camera used for
    viewing the scene rendered by the raytracer.
*/
class Camera {
    private:
        // Direction the camera is facing.
        Vector direction;
        // Location of the camera in the scene
        Vector location;
        // Up vector of the camera
        Vector cameraUp;
        // Right vector of the camera
        Vector cameraRight;
        // Distance the camera reaches
        float distance;
        // Field of view
        float fov;
    public:
        //! Constructor for the class.
        /*!
            The Camera constructor takes a Vector of desired position of the
            camera, a vector at which the camera should be pointing at, and an
            'up' vector specifying the orientation of the camera.
            The optional parameter f describes the focal angular width of the
            camera in degrees.
        */
        Camera(Vector position, Vector lookAt, Vector up, float f = 60);
        //! A utility function for converting pixels on screen into rays to be
        //! shot for the raytracer.
        Ray getRayForPixel(int x, int y, int imgSize) const;
        //! Returns the direction the camera's pointing at.
        const Vector getDirection() const;
        //! Returns the location of the camera.
        const Vector getLocation() const;
        //! Returns the unit vector pointing upwards relative to camera's view.
        const Vector getUp() const;
        //! Returns the unit vector pointing right relative to camera's view.
        const Vector getRight() const;
        //! Returns the distance parameter of the camera.
        const float getDistance() const;
        //! Returns the field of view constant of the camera.
        const float getFieldOfView() const;
};
#endif