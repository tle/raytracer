#ifndef PLANE_H
#define PLANE_H

#include "SceneObject.h"

class Plane: public SceneObject {
    private:
        Vector surfaceNormal;
        float distance;
    public:
        Plane(const Color &c, const Vector &n, float d, float r) :
            SceneObject(c, r), surfaceNormal(n.unit()), distance(d) {  };    
        float intersection(const Ray &r) const;
        const Vector getNormalAtPoint(const Vector &point) const;
        const Color getColorAtPoint(const Vector &point) const;
};

#endif