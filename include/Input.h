#include <istream>
#include <map>

#include "Color.h"
#include "Vector.h"
#include "Camera.h"

#include "SceneObject.h"
#include "Sphere.h"
#include "Plane.h"
#include "Triangle.h"
#include "Light.h"

using namespace std;

istream & operator>>(istream &is, Color &col);
istream & operator>>(istream &is, Vector &vec);

typedef SceneObject* (*SceneObjectReader)(istream &is);

SceneObject* ReadSphere(istream &is);
SceneObject* ReadPlane(istream &is);
SceneObject* ReadTriangle(istream &is);

Light* ReadLight(istream &is);
Camera* ReadCamera(istream &is);

