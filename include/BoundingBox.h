#ifndef BOUNDING_BOX_H
#define BOUNDING_BOX_H

#include "Vector.h"
#include "Ray.h"

class BoundingBox {
	public:
		// The box is defined by two vertices diagonal from each other
		BoundingBox(const Vector &v0, const Vector &v1) { 
			bounds[0] = v0;
			bounds[1] = v1;
		};

		Vector bounds[2];

		const bool intersect(Ray &r) const;

};

#endif