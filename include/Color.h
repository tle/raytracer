#ifndef COLOR_H
#define COLOR_H

#include <stdexcept>
#include <ostream>
#include <limits>
#include <cmath>


//! Color class used for defining colors in RGB color space.
/*!
    The color class supports arithmetic operations. Allowed operations can
    involve colors and scalars.
    The color class clamps its fields between 0 and 1 - should any operation
    exceed that range, the Color class will clamp the values.
*/
class Color {
    private:
        float r, g, b;
        bool valid(float value) const;
        float clip(float value) const;        
    public:
        //! Default constructor, initializes the color to (0, 0, 0) - black.
        Color() : r(0), g(0), b(0) { } ;
        //! Constructor for specifying the RGB components between 0 and 1.
        Color(float red, float green, float blue);
        //! Copy constructor.
        Color(const Color &c);
        
        //! Accessor for the Red component of the color.
        
        const float getRed() const;
        //! Accessor for the Green component of the color.
        const float getGreen() const;
        //! Accessor for the Blue component of the color.
        const float getBlue() const;
        
        //! Mutator for the red component of the color.
        /*!
        * Throws std::invalid_argument if the value is not between 0 and 1
        */
        void setRed(float value);
        //! Mutator for the green component of the color.
        /*!
        * Throws std::invalid_argument if the value is not between 0 and 1
        */
        void setGreen(float value);
        //! Mutator for the blue component of the color
        /*!
        * Throws std::invalid_argument if the value is not between 0 and 1
        */
        void setBlue(float value);
        
        bool operator==(const Color &c) const;
        
        Color & operator=(const Color &c);
        Color & operator+=(const Color &c);
        Color & operator*=(const Color &c);
        Color & operator-=(const Color &c);
        
        Color & operator+=(float s);
        Color & operator-=(float s);
        Color & operator*=(float s);
        Color & operator/=(float s);
        
        
        const Color operator+(const Color &c) const;
        const Color operator-(const Color &c) const;
        const Color operator*(const Color &c) const;
        };

const Color operator+(const Color &c, float s);
const Color operator+(float s, const Color &c);

const Color operator*(const Color &c, float s);
const Color operator*(float s, const Color &c);

const Color operator/(const Color &c, float s);

std::ostream & operator<<(std::ostream &os, const Color &c);
#endif