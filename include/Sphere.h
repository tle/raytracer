#ifndef SPHERE_H
#define SPHERE_H
#include "SceneObject.h"

class Sphere: public SceneObject {
    private:
        Vector center;
        float radius;
    public:
        Sphere(const Color &c, const Vector &cen, float rad, float ref) :
            SceneObject(c, ref), center(cen), radius(rad) { };
        int getIntersections(const Ray &r, float &t1, float &t2) const;
        float intersection(const Ray &r) const;
        const Vector getNormalAtPoint(const Vector &point) const;
        const Color getColorAtPoint(const Vector &point) const;
};

#endif