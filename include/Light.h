#ifndef LIGHT_H
#define LIGHT_H
//! A class for describing lights in the raytraced scene.
class Light {
    private:
        Vector position;
        Color color;
    public:
        //! Default constructor
        /*!
        * Initializes a black light at (0, 0, 0)
        */
        Light() : position(Vector()), color(Color()) { };
        //! Constructor for specifying light position and color.
        Light(Vector pos, Color col) : position(pos), color(col) { };
        //! Returns the position of the light.
        const Vector getPosition() const{
            return position;
        }
        //! Returns the color of the light.
        const Color getColor() const {
            return color;
        }
};
#endif