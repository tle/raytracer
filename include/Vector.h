#ifndef VECTOR_H
#define VECTOR_H

#include <cassert>
#include <cmath>
#include <iostream>
#include <stdexcept>

class Vector {
    public:
        //! Vector x coordinate.
        float x;
        //! Vector y coordinate.
        float y;
        //! Vector z coordinate.
        float z;
        //! Default constructor initializes a (0, 0, 0) vector.
        Vector() : x(0), y(0), z(0) { }
        Vector(float a, float b, float c) : x(a), y(b), z(c) { }
        //! Copy constructor.
        Vector(const Vector &v) : x(v.x), y(v.y), z(v.z) { }
        
        //! Equality operator.
        bool operator==(const Vector &v) const;
        bool operator!=(const Vector &v) const;
        
        // Self assignment operations on Scalars
        // addition
        Vector & operator+=(float number);
        // subtraction
        Vector & operator-=(float number);
        // division
        Vector & operator/=(float number);
        // mulutiplication
        Vector & operator*=(float number);
        
        // Self assignment operations on Vectors
        Vector & operator=(const Vector &v);
        Vector & operator+=(const Vector &v);
        Vector & operator-=(const Vector &v);
        
        //! Returns the length of the vector.
        float norm() const;
        //! Returns unit vector in the direction of the vector.
        /*!
        *   Throws <b>std::length_error</b> if the length of the vector is 0.
        */
        Vector unit() const;
        //! Returns whether given vector is a unit vector
        /*!
        *   Throws <b>std::length_error</b> if the length of the vector is 0.
        */
        bool isUnit() const;
        
};

// Vector-Scalar and Scalar-Vector operations
// Scalar addition
const Vector operator+(const Vector &v, float s);
const Vector operator+(float s, const Vector &v);
// Scalar subtraction
const Vector operator-(const Vector &v, float s);
const Vector operator-(float s, const Vector &v);
// Scalar multiplication
const Vector operator*(const Vector &v, float s);
const Vector operator*(float s, const Vector &v);
// Scalar division (only right)
const Vector operator/(const Vector &v, float s);


// Vector - Vector operations
// Addition
const Vector operator+(const Vector &v, const Vector &s);
// Subtraction
const Vector operator-(const Vector &v, const Vector &s);
// Dot product
float dot(const Vector &u, const Vector &v);
// Cross product
const Vector cross(const Vector &u, const Vector &v);
// Vector projection
const Vector project(const Vector &u, const Vector &v);

// Vector negation
const Vector operator-(const Vector &v);

// Printing
std::ostream& operator<<(std::ostream& os, const Vector &v);
#endif