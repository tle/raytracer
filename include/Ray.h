#ifndef RAY_H
#define RAY_H

#include "Vector.h"
#include <limits>
//! A class for describing Rays used by the raytracer for rendering the scene.
class Ray {
    private:
        static const float REFLECTION_DELTA = 0.0001;
    public:
        //! Class constructor
        /*!
        * Creates a ray at given origin in a given direction.
        * The flag normalize specifies whether we want to normalize the
        * direction vector.
        */
        Ray(const Vector &orig, const Vector &dir, bool normalize = true);
        //! Origin of the ray.
        Vector origin;
        //! Direction of the ray.
        Vector direction;
        //! Inverse direction of the ray
        Vector invDirection;
        //! Max and min t of the ray
        float tMin, tMax;
        //! Sign of the ray in each direction
        int sign[3];
        //! Returns the point along the ray at coordinate t.
        /*!
        *   The point returned is described by the equation:
        *   <b> P(t) = origin + t * direction </b>
        */
        const Vector getPoint(float t) const;
        //! Reflects the ray at the specified location against the given normal.
        const Ray reflect(const Vector &location, Vector &normal) const;
};
#endif