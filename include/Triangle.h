#ifndef TRIANGLE_H
#define TRIANGLE_H
#include "SceneObject.h"

class Triangle: public SceneObject {
    private:
        // the three vertices of the triangle
        Vector A;
        Vector B;
        Vector C;
        // normal vector
        Vector N;
    public:
        Triangle(const Color &c, const Vector &p0, const Vector &p1, const Vector &p2,
                float ref) : 
            SceneObject(c, ref), A(p0), B(p1), C(p2) {
                N = cross(B - A, C - A).unit();
            };
        float intersection(const Ray &r) const;
        const Vector getNormalAtPoint(const Vector &point) const;
        const Color getColorAtPoint(const Vector &point) const;
};

#endif