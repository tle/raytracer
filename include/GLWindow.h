#ifndef GLWINDOW_H
#define GLWINDOW_H
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glfw.h>

//! A GLWindow class for displaying CUDA output.
/*!
*   Currently unused.
*/
class GLWindow {
    private:
        int width;
        int height;
        void initGL();
        int checkRun();
        virtual void redraw();
    public:
        //! Class constructor
        /*!
        * Initializes a GLWindow of given width and height.
        */
        GLWindow(int w, int h) : width(w), height(h) { }
        //! Runs OpenGL initialization routines.
        void init(int argc, char *argv[]);
        //! Starts the OpenGL window loop.
        void loop();
};       
#endif