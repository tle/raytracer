#ifndef SCENE_H
#define SCENE_H

#include "SceneObject.h"
#include "Light.h"
#include "Camera.h"

#include <stdexcept>
#include <algorithm>
#include <ostream>
#include <vector>
#include <limits>
#include <math.h>

using namespace std;

#ifndef DOXYGEN_SHOULD_SKIP_THIS
typedef std::vector<SceneObject *>::const_iterator objectIter;
typedef std::vector<Light *>::const_iterator lightIter;

struct DeleteObject { 
    template<typename T> 
    void operator()(const T *ptr) const { 
        delete ptr; 
    } 
};
#endif

//! Scene rendered by the raytracer
class Scene {
    private:
        vector<SceneObject *> objects;
        vector<Light *> lights;
        const Color backgroundColor;
        static const int MAX_DEPTH = 6;
    public:
        //! Creates an empty, black scene.
        Scene() : backgroundColor(Color(0, 0, 0)) { };
        //! Creates a scene with given Color.
        Scene(const Color &col) : backgroundColor(Color(col)) { };
        //! Creates a scene with background color of given coordinates
        Scene(float r, float g, float b) : backgroundColor(Color(r, g, b)) { };
        ~Scene();
        //! Adds a SceneObject to the scene.
        void addObject(SceneObject *obj);
        //! Adds a light to the scene
        void addLight(Light *lt);
        
        //! Finds the closest object in the scene that intersects given ray.
        /*!
        *   Finds the closest object in the scene by iterating through objects
        *   in the scene.
        */
        SceneObject * findClosestObject(const Ray &r, float &tIntersect) const;
        
        //! Finds the color of the ray in the scene.
        const Color raytrace(const Ray &r, int depth = 0) const;
        //! Renders the scene.
        /*!
        *   Using the specified Camera object, the function computes the NDC
        *   pixel coordinates and shoots rays through the scene to render
        *   the image to given output stream.
        */
        void render(const Camera &cam, int imageSize, ostream &os) const;
};
#endif