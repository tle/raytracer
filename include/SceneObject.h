#ifndef SCENE_OBJECT_H
#define SCENE_OBJECT_H

#include "Color.h"
#include "Vector.h"
#include "Ray.h"

//! General class for Scene objects
class SceneObject {
    protected:
        Color surfaceColor;
        float reflectivity;
    public:
        static enum {NO_INTERSECTION = -1} ERROR;
        SceneObject() : surfaceColor(Color(0.5, 0.5, 0.5)), reflectivity(0) { };
        SceneObject(const Color &c, float r) :
            surfaceColor(c), reflectivity(r) { };
        void setColor(const Color &c)  {
            surfaceColor = c;
        };
        const Color getColor() const {
            return surfaceColor;
        };
        
        const float getReflectivity() const {
            return reflectivity;
        };
        
        virtual float intersection(const Ray &r) const = 0;
        virtual const Vector getNormalAtPoint(const Vector &point) const = 0;
        virtual const Color getColorAtPoint(const Vector &point) const = 0;
};
#endif