#include "Vector.h"
#include "Ray.h"
#include "Triangle.h"
#include <gtest/gtest.h>
#include <iostream>

using namespace testing;

TEST(TriangleTest, intersection) {
    Ray r(Vector(), Vector(1, 0, 0));
    Vector p1(5, 1, 0);
    Vector p2(5, -1, -1);
    Vector p3(5, -1, 1);
    Triangle t(Color(), p1, p2, p3, 0);
    EXPECT_FLOAT_EQ(t.intersection(r), 5.0f);
}

TEST(TriangleTest, normal) {
    Ray r(Vector(), Vector(1, 0, 0));
    Vector p1(5, 1, 0);
    Vector p2(5, -1, -1);
    Vector p3(5, -1, 1);
    Triangle t(Color(), p1, p2, p3, 0);
    Vector point = r.getPoint(t.intersection(r));
    EXPECT_EQ(t.getNormalAtPoint(point), Vector(-1, 0, 0));
}