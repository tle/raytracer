#include "SceneObject.h"
#include "Scene.h"
#include "Sphere.h"
#include "Plane.h"

#include <gtest/gtest.h>
#include <iostream>

TEST(SceneTest, ClosestObjectTest) {
    Ray r = Ray(Vector(), Vector(1, 0, 0));
    Sphere s(Color(1, 0, 0), Vector(3,0,0), 1, 0);
    Scene sc;
    sc.addObject(&s);
    float t;
    SceneObject *ptr = sc.findClosestObject(r, t);
    EXPECT_FLOAT_EQ(t, 2.0f);
    EXPECT_FALSE(t == 5.0f);
    EXPECT_TRUE(ptr->getColor() == Color(1, 0, 0));
}

TEST(SceneTest, MultipleClosestObjectTest) {
    Ray r(Vector(), Vector(1, 0, 0));
    Sphere *s = new Sphere(Color(1, 0, 0), Vector(2,0,0), 1, 0);

    Plane *p = new Plane(Color(0, 1, 0), Vector(-1, 0, 0), 0.5, 0);
    Scene sc;
    sc.addObject(s);
    sc.addObject(p);
    float t;
    SceneObject *ptr = sc.findClosestObject(r, t);
    EXPECT_FLOAT_EQ(t, 0.5f);
    EXPECT_FALSE(t == 5.0f);
    EXPECT_EQ(ptr->getColor(), Color(0, 1, 0));
    Sphere *s2 = new Sphere(Color(0, 0, 1), Vector(0.25,0,0), 0.25, 0);
    sc.addObject(s2);
    ptr = sc.findClosestObject(r, t);
    EXPECT_EQ(ptr->getColor(), Color(0, 0, 1));
}

TEST(SceneTest, RaytraceTest) {
    Ray r = Ray(Vector(), Vector(1, 0, 0));
    Sphere *s = new Sphere(Color(1, 0, 0.5), Vector(2,0,0), 1, 0);
    Scene sc;
    sc.addObject(s);
    Light *l = new Light(Vector(0, 0, 0), Color(1, 0, 0));
    sc.addLight(l);
    EXPECT_TRUE(sc.raytrace(r) == Color(1, 0, 0));
    Light *l2 = new Light(Vector(0, 0, 0), Color(0, 0, 0.3));
    sc.addLight(l2);
    EXPECT_TRUE(sc.raytrace(r) == Color(1, 0, 0.045));
}