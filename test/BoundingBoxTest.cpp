#include "BoundingBox.h"
#include "Ray.h"
#include <gtest/gtest.h>

using namespace testing;

TEST(BoundingBoxTest, intersectionTest) {
    Vector v1(-1, -1, -1);
    Vector v2(1, 1, 1);

    BoundingBox bb(v1, v2);

    Ray r(Vector(-5, 0, 0), Vector(1, 0, 0));
    Ray r2(Vector(-5, 0, 0), Vector(1, 1, 0), true);
    EXPECT_TRUE(bb.intersect(r));
    EXPECT_FALSE(bb.intersect(r2));
    EXPECT_EQ(r.tMin, 4);
    EXPECT_EQ(r.tMax, 6);
}
