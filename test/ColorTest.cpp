#include "Color.h"
#include <gtest/gtest.h>
#include <iostream>

using namespace testing;

// Test initialization
TEST(ColorTest, BasicTest) {
    Color c(0.1, 0.3, 0.2);
    EXPECT_FLOAT_EQ(c.getRed(), 0.1f);
    EXPECT_FLOAT_EQ(c.getGreen(), 0.3f);
    EXPECT_FLOAT_EQ(c.getBlue(), 0.2f);
    Color u;
    EXPECT_FLOAT_EQ(u.getRed(), 0.0f);
    EXPECT_FLOAT_EQ(u.getGreen(), 0.0f);
    EXPECT_FLOAT_EQ(u.getBlue(), 0.0f);
    Color z(c);
    EXPECT_FLOAT_EQ(z.getRed(), 0.1f);
    EXPECT_FLOAT_EQ(z.getGreen(), 0.3f);
    EXPECT_FLOAT_EQ(z.getBlue(), 0.2f);
    
    EXPECT_THROW(Color(2, 0, 0), std::invalid_argument);
    
    EXPECT_TRUE(z == c);
}

TEST(Color, AdditionTest) {
    Color c(0.1, 0.3, 0.2);
    Color f(0.2, 0.5, 0.0);
    
    Color t(0.3, 0.8, 0.2);
    EXPECT_TRUE(t == c + f);
}

TEST(Color, SubtractionTest) {
    Color c(0.1, 0.3, 0.2);
    Color f(0.2, 0.5, 0.0);
    
    Color t(0.1, 0.2, 0.0);
    EXPECT_TRUE(t == f - c);
}


TEST(Color, MultiplicationTest) {
    Color c(0.1, 0.3, 0.2);
    Color f(0.2, 0.5, 0.0);
    
    Color t(0.02, 0.15, 0.0);
    EXPECT_FLOAT_EQ((c * f).getRed(), t.getRed());
    EXPECT_FLOAT_EQ((c * f).getGreen(), t.getGreen());
    EXPECT_FLOAT_EQ((c * f).getBlue(), t.getBlue());
    EXPECT_TRUE(t == c * f);
}

TEST(Color, DivisionTest) {
    Color c(0.1, 0.3, 0.2);
    c /= 2;
    Color t(0.05, 0.15, 0.1);
    EXPECT_TRUE(t == c);
    EXPECT_THROW(c / 0, std::invalid_argument);
}


TEST(ColorTest, PrintingTest) {
    Color u(1, 0, 0);
    std::cout << u;
}