#include "Input.h"

#include <gtest/gtest.h>
#include <iostream>
#include <sstream>
#include <string>

using namespace testing;

TEST(InputTest, vectorInput) {
    stringstream teststream;
    teststream << "(-10, 10, 5)\n";
    Vector v;
    teststream >> v;
    EXPECT_EQ(v, Vector(-10, 10, 5));
}

TEST(InputTest, colorInput) {
    stringstream teststream;
    teststream << "[0.3, 0.0, 0.1]\n";
    Color c;
    teststream >> c;
    EXPECT_EQ(c, Color(0.3, 0.0, 0.1));
}

