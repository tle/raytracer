#include "Vector.h"
#include "Ray.h"
#include "Sphere.h"
#include <gtest/gtest.h>
#include <iostream>

using namespace testing;

TEST(SphereTest, simpleIntersection) {
    Ray r = Ray(Vector(), Vector(1, 0, 0));
    Sphere s(Color(), Vector(2,0,0), 1, 0);
    EXPECT_FLOAT_EQ(s.intersection(r), 1.0);
    Sphere s2(Color(), Vector(), 1, 0);
    EXPECT_FLOAT_EQ(s2.intersection(r), 1);
}

TEST(SphereTest, bothIntersections) {
    Ray r = Ray(Vector(0, 0, 0), Vector(1, 0, 0));
    Sphere s(Color(), Vector(2,0,0), 1, 0);
    float t1, t2;
    s.getIntersections(r, t1, t2);
    EXPECT_FLOAT_EQ(s.intersection(r), 1.0);
    EXPECT_EQ(r.getPoint(t1), Vector(1, 0, 0));
    EXPECT_EQ(r.getPoint(t2), Vector(3, 0, 0));
}

TEST(SphereTest, noIntersections) {
    Ray r = Ray(Vector(0, 0, 0), Vector(-1, 0, 0));
    Sphere s(Color(), Vector(2,0,0), 1, 0);
    float t1, t2;
    s.getIntersections(r, t1, t2);
    EXPECT_FLOAT_EQ(t1, -1);
    EXPECT_FLOAT_EQ(t2, -1);
}

TEST(SphereTest, oneIntersection) {
    Ray r = Ray(Vector(0, 0, 0), Vector(1, 0, 0));
    Sphere s(Color(), Vector(2, 1, 0), 1, 0);
    float t1, t2;
    s.getIntersections(r, t1, t2);
    EXPECT_FLOAT_EQ(t1, 2);
    EXPECT_FLOAT_EQ(t2, -1);
}

TEST(SphereTest, normal) {
    Ray r = Ray(Vector(), Vector(1, 0, 0));
    Sphere s(Color(), Vector(2,0,0), 1, 0);
    Vector p = r.getPoint(s.intersection(r));
    EXPECT_TRUE(s.getNormalAtPoint(p) == Vector(-1, 0, 0));
}