#include "Vector.h"
#include "Ray.h"
#include "Plane.h"
#include <gtest/gtest.h>
#include <iostream>

using namespace testing;

TEST(PlaneTest, intersection) {
    Ray r(Vector(), Vector(1, 0, 0));
    Plane p(Color(), Vector(-1, 0, 0), 5, 0);
    Plane p2(Color(), Vector(-1, 1, 0), 3.0f, 0);
    Plane p3(Color(), Vector(-5, 0, 0), 3, 0);
    EXPECT_FLOAT_EQ(p.intersection(r), 5.0f);
    EXPECT_FLOAT_EQ(p2.intersection(r), 3.0 * sqrt(2.0f));
    EXPECT_FLOAT_EQ(p3.intersection(r), 3.0f);
}

TEST(PlaneTest, normal) {
    Ray r(Vector(), Vector(1, 0, 0));
    Plane p(Color(), Vector(-1, 0, 0), 5, 0);
    Vector point = r.getPoint(p.intersection(r));
    EXPECT_TRUE(p.getNormalAtPoint(point) == Vector(-1, 0, 0));
}