#include "Vector.h"
#include <gtest/gtest.h>
#include <iostream>

using namespace testing;

// Test initialization
TEST(VectorTest, BasicTest) {
    Vector v(3, 6, 9);
    EXPECT_FLOAT_EQ(v.x, 3.0f);
    EXPECT_FLOAT_EQ(v.y, 6.0f);
    EXPECT_FLOAT_EQ(v.z, 9.0f);
    Vector u;
    EXPECT_FLOAT_EQ(u.x, 0.0f);
    EXPECT_FLOAT_EQ(u.y, 0.0f);
    EXPECT_FLOAT_EQ(u.z, 0.0f);
    Vector z(v);
    EXPECT_FLOAT_EQ(z.x, 3.0f) << "Error in copy initializer!";
    EXPECT_FLOAT_EQ(z.y, 6.0f) << "Error in copy initializer!";
    EXPECT_FLOAT_EQ(z.z, 9.0f) << "Error in copy initializer!";
    
    Vector w;
    w = v;
    EXPECT_TRUE(w == v);
    EXPECT_FALSE(w != v);
    v.x = 1.0f;
    EXPECT_FALSE(w == v);
    EXPECT_TRUE(w != v);
}



TEST(VectorTest, AdditionTest) {
    Vector v(3, 6, 9);
    Vector u(1, -2, 3);
    float s = 2.0f;
    Vector w;
    w = u + v;
    EXPECT_FLOAT_EQ(w.x, 4.0f);
    EXPECT_FLOAT_EQ(w.y, 4.0f);
    EXPECT_FLOAT_EQ(w.z, 12.0f);
    w += s;
    EXPECT_FLOAT_EQ(w.x, 6.0f);
    EXPECT_FLOAT_EQ(w.y, 6.0f);
    EXPECT_FLOAT_EQ(w.z, 14.0f);
    w = v + s;
    EXPECT_FLOAT_EQ(w.x, 5.0f);
    EXPECT_FLOAT_EQ(w.y, 8.0f);
    EXPECT_FLOAT_EQ(w.z, 11.0f);
    w = s + v;
    EXPECT_FLOAT_EQ(w.x, 5.0f);
    EXPECT_FLOAT_EQ(w.y, 8.0f);
    EXPECT_FLOAT_EQ(w.z, 11.0f);
}

TEST(VectorTest, SubtractionTest) {
    Vector v(3, 6, 9);
    Vector u(1, -2, 3);
    float s = 2.0f;
    Vector w;
    w = v - u;
    EXPECT_FLOAT_EQ(w.x, 2.0f);
    EXPECT_FLOAT_EQ(w.y, 8.0f);
    EXPECT_FLOAT_EQ(w.z, 6.0f);
    w -= s;
    EXPECT_FLOAT_EQ(w.x, 0.0f);
    EXPECT_FLOAT_EQ(w.y, 6.0f);
    EXPECT_FLOAT_EQ(w.z, 4.0f);
    w = v - s;
    EXPECT_FLOAT_EQ(w.x, 1.0f);
    EXPECT_FLOAT_EQ(w.y, 4.0f);
    EXPECT_FLOAT_EQ(w.z, 7.0f);
    w = s - u;
    EXPECT_FLOAT_EQ(w.x, 1.0f);
    EXPECT_FLOAT_EQ(w.y, 4.0f);
    EXPECT_FLOAT_EQ(w.z, -1.0f);
}


TEST(VectorTest, MultiplicationTest) {
    Vector v(3, 6, 9);
    float s = 2.0f;
    Vector w;
    w = s * v;
    EXPECT_FLOAT_EQ(w.x, 6.0f);
    EXPECT_FLOAT_EQ(w.y, 12.0f);
    EXPECT_FLOAT_EQ(w.z, 18.0f);
    w = v * s;
    EXPECT_FLOAT_EQ(w.x, 6.0f);
    EXPECT_FLOAT_EQ(w.y, 12.0f);
    EXPECT_FLOAT_EQ(w.z, 18.0f);
    w = v;
    w *= s;
    EXPECT_FLOAT_EQ(w.x, 6.0f);
    EXPECT_FLOAT_EQ(w.y, 12.0f);
    EXPECT_FLOAT_EQ(w.z, 18.0f);
}

TEST(VectorTest, DivisionTest) {
    Vector v(3, 6, 9);
    float s = 0.5f;
    Vector w;
    w = v / s;
    EXPECT_FLOAT_EQ(w.x, 6.0f);
    EXPECT_FLOAT_EQ(w.y, 12.0f);
    EXPECT_FLOAT_EQ(w.z, 18.0f);
    s = 0.0f;
    EXPECT_THROW(v / s, std::invalid_argument);
}

TEST(VectorTest, CrossTest) {
    Vector u(3, 6, 9);
    Vector v(4, 2, -1);
    Vector w = cross(u, v);
    EXPECT_FLOAT_EQ(w.x, -24.0f);
    EXPECT_FLOAT_EQ(w.y, 39.0f);
    EXPECT_FLOAT_EQ(w.z, -18.0f);
    w = cross(v, u);
    EXPECT_FLOAT_EQ(w.x, 24.0f);
    EXPECT_FLOAT_EQ(w.y, -39.0f);
    EXPECT_FLOAT_EQ(w.z, 18.0f);
    w = cross(v, v);
    EXPECT_FLOAT_EQ(w.x, 0.0f);
    EXPECT_FLOAT_EQ(w.y, 0.0f);
    EXPECT_FLOAT_EQ(w.z, 0.0f);
}

TEST(VectorTest, DotTest) {
    Vector u(3, 6, 9);
    Vector v(4, 2, -1);
    float result = dot(u, v);
    EXPECT_FLOAT_EQ(result, 15.0f);
}


TEST(VectorTest, NormTest) {
    Vector u(3, 6, 9);
    EXPECT_FLOAT_EQ(u.norm(), 11.22497f);
    Vector v;
    EXPECT_FLOAT_EQ(v.norm(), 0.0f);
    Vector w(-1.0, 0.0, 0.0);
    EXPECT_FLOAT_EQ(w.norm(), 1.0f);
    EXPECT_TRUE(w.isUnit());
}


TEST(VectorTest, UnitVectorTest) {
    Vector u(2, 0, 0);
    EXPECT_NO_THROW(u.unit());
    EXPECT_EQ(u.unit(), Vector(1, 0, 0));
    Vector v;
    EXPECT_THROW(v.unit(), std::length_error);
}

TEST(VectorTest, ProjectTest) {
    Vector u(1, 1, 0);
    Vector v(3, 0, 0);
    EXPECT_EQ(project(u, v), Vector(1, 0, 0));
    

}

TEST(VectorTest, PrintingTest) {
    Vector u(2, 0, 0);
    std::cout << u;
}