#include "Input.h"

istream & operator>>(istream &s, Color &col) {
	float r, g, b;

	char ch = 0;
	if (!s) return s;

	s >> ch;
	if (ch == '[') {
		s >> r >> ch;
		s >> g >> ch;
		s >> b >> ch;
		if (ch != ']') s.clear(ios_base::failbit);
	}

	if (s) col = Color(r, g, b);

	return s;
}

istream & operator>>(istream &s, Vector &v) {
	float x, y, z;

	char ch = 0;
	if (!s) return s;

	s >> ch;

	if (ch == '(') {
		s >> x >> ch;
		s >> y >> ch;
		s >> z >> ch;
		if (ch != ')') s.clear(ios_base::failbit);
	}

	if (s)
		v = Vector(x, y, z);
	return s;
}


SceneObject* ReadSphere(istream &is) {
	Color col;
	float radius;
	float reflectivity;
	Vector center;

	is >> center;
	is >> radius;
	is >> col;
	is >> reflectivity;

	return new Sphere(col, center, radius, reflectivity);
}

SceneObject* ReadPlane(istream &is) {
	Color col;
	float distance;
	Vector normal;
	float reflectivity;

	is >> normal;
	is >> distance;
	is >> col;
	is >> reflectivity;

	return new Plane(col, normal, distance, reflectivity);
}

SceneObject* ReadTriangle(istream &is) {
	Color col;
	Vector p1;
	Vector p2;
	Vector p3;
	float reflectivity;

	is >> p1;
	is >> p2;
	is >> p3;
	is >> col;
	is >> reflectivity;

	return new Triangle(col, p1, p2, p3, reflectivity);
}

Camera* ReadCamera(istream &is) {
	Vector position;
	Vector lookAt;
	Vector up;
	is >> position;
	is >> lookAt;
	is >> up;
	return new Camera(position, lookAt, up);
}

Light* ReadLight(istream &is) {
	Vector position;
	Color col;
	is >> position;
	is >> col;
	return new Light(position, col);
}

