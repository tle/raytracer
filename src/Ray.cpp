#include "Ray.h"

Ray::Ray(const Vector &orig, const Vector &dir, bool normalize) {
    origin = orig;
    if (normalize)
        direction = dir.unit();
    else
        direction = dir;

    tMin = 0;
    tMax = std::numeric_limits<float>::max();

    invDirection.x = 1.0f / direction.x;
    invDirection.y = 1.0f / direction.y;
    invDirection.z = 1.0f / direction.z;

    sign[0] = invDirection.x < 0;
    sign[1] = invDirection.y < 0;
    sign[2] = invDirection.z < 0;
}

const Vector Ray::getPoint(float t) const {
    return origin + t * direction;
}

const Ray Ray::reflect(const Vector &location, Vector &normal) const{
    Vector Dpar = project(-direction, normal);
    Vector new_dir = direction + 2.0f * Dpar;
    return Ray(location + new_dir * REFLECTION_DELTA, new_dir, true);
}
