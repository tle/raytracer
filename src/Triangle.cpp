#include "Triangle.h"

// formulas taken from
// http://www.cs.washington.edu/education/courses/cse457/07sp/lectures/triangle_intersection.pdf
float Triangle::intersection(const Ray &r) const {
	float dist = dot(N, A);
	float t = dist - dot(N, r.origin);
	float nd = dot(N, r.direction);
	if (nd == 0)
		return NO_INTERSECTION;
	t /= nd;
	Vector Q = r.getPoint(t);

	bool intersects = dot(cross(B - A, Q - A), N) >= 0;
	intersects &= dot(cross(C - B, Q - B), N) >= 0;
	intersects &= dot(cross(A - C, Q - C), N) >= 0;

	if (intersects)
		return t;
	else
		return NO_INTERSECTION;

}

const Vector Triangle::getNormalAtPoint(const Vector &point) const {
	return N;
}

const Color Triangle::getColorAtPoint(const Vector &point) const {
	return surfaceColor;
}