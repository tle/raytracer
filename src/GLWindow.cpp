#include "GLWindow.h"

void GLWindow::initGL() {
    // Flags for some features
    // Shading
    glShadeModel(GL_FLAT);
    // Depth Test
    glEnable(GL_DEPTH_TEST);
    // Lighting
    glDisable(GL_LIGHTING);

    // Scene set up
    // Set up the projection matrix
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(-1.0, 1.0, -1.0, 1.0, 0.1, 100.0);
    // Reset the modelview matrix
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


void GLWindow::init(int argc, char *argv[]) {
    glfwInit();
    glfwOpenWindow(width, height, 0, 0, 0, 0, 0, 0, GLFW_WINDOW);
    initGL();
}

// Utility functions for UI
// check whether to continue looping
int GLWindow::checkRun() {
    // check if ESC pressed
    int esc = !glfwGetKey( GLFW_KEY_ESC);
    // check if window is opened
    int opened = glfwGetWindowParam( GLFW_OPENED );
    return esc && opened;
}

void GLWindow::loop() {
    int running = GL_TRUE;
    while (running) {
        redraw();
        running = checkRun();
    }
    glfwTerminate();    
}

void GLWindow::redraw() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glMatrixMode(GL_MODELVIEW_MATRIX);
    glLoadIdentity();

    // enable texturing
    // glEnable(GL_TEXTURE_2D);
    // draw the primitive
    glTranslatef(0, 0, -3.0);
    glColor3f(1.0, 1.0, 1.0);
    glBegin(GL_QUADS);
    
    glTexCoord2f(0.0, 0.0);
    glVertex3f(-1.0, 1.0, 0.0);

      
    glTexCoord2f(1.0, 0.0);
    glVertex3f(1.0, 1.0, 0.0);

   
    glTexCoord2f(1.0, 1.0);
    glVertex3f(1.0,-1.0, 0.0);

   
    glTexCoord2f(0.0, 1.0);
    glVertex3f(-1.0, -1.0, 0.0);

    glEnd();

    glfwSwapBuffers();
}
