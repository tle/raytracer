#include <iostream>
// #include "GLWindow.h"
#include "Scene.h"
#include "Input.h"
#include <iostream>

map<string, SceneObjectReader> readFuncs; 

int main(int argc, char *argv[]) {
    readFuncs["sphere"] = ReadSphere; 
    readFuncs["plane" ] = ReadPlane;
    readFuncs["triangle" ] = ReadTriangle;


    Scene scene;
    Camera *cam;
    string inputLine;

    string type;

    while (std::cin >> type) {
        if (readFuncs.find(type) != readFuncs.end())
        {
            SceneObject *newObj;
            newObj = readFuncs[type](std::cin);
            scene.addObject(newObj);
        }
        else if (type == "light")
        {
            Light *newLight;
            newLight = ReadLight(std::cin);
            scene.addLight(newLight);
        }
        else if (type == "camera")
        {
            cam = ReadCamera(std::cin);
        }
        else
        {
            std::cout<< "An error occurred." << std::endl;
        }
    }

    if (argc == 2)
        scene.render(*cam, atoi(argv[1]), std::cout);
    else
        scene.render(*cam, 400, std::cout);
    return 0;
}
