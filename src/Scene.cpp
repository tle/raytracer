#include "Scene.h"

using namespace std;

Scene::~Scene() {
    for_each(objects.begin(), objects.end(), DeleteObject());
    for_each(lights.begin(), lights.end(), DeleteObject());
}

void Scene::addObject(SceneObject *obj) {
    if (obj == 0) {
        throw(invalid_argument("Null pointer to object!"));
    }
    objects.push_back(obj);
}

void Scene::addLight(Light *lt) {
    if (lt == 0) {
        throw(invalid_argument("Null pointer to light!"));
    }
    lights.push_back(lt);
}

SceneObject * Scene::findClosestObject(const Ray &r, float &tintersect) const{
    objectIter iter;
    SceneObject *result = 0;
    tintersect = numeric_limits<float>::infinity();
    for (iter = objects.begin(); iter != objects.end(); iter++) {
        float t = (*iter)->intersection(r);
        if (t < tintersect && t != SceneObject::NO_INTERSECTION) {
            result = *iter;
            tintersect = t;
        }
    }
    return result;
}

const Color Scene::raytrace(const Ray &r, int depth) const {
    float t;
    SceneObject *closest = findClosestObject(r, t);
    // no intersection found if pointer obtained is null
    Color result(0, 0, 0);
    if (closest == 0) {
        result = backgroundColor;
    }
    else {
        Vector intersectLoc = r.getPoint(t);
        Vector N = closest->getNormalAtPoint(intersectLoc);
        
        // reflected rays
        if (closest->getReflectivity() != 0.0f && depth <= MAX_DEPTH) {
                Ray reflectedRay = r.reflect(intersectLoc, N);
                Color reflectionColor = raytrace(reflectedRay, depth + 1);
                result += closest->getReflectivity() * reflectionColor;
        }

        Color objectColor = closest->getColorAtPoint(intersectLoc);
        // ambient lighting
        Color ambient = objectColor;
        Color diffuse, specular;
        // object color itself
        for (lightIter li = lights.begin(); li != lights.end(); li++) {
            Vector lightLoc = (*li)->getPosition();
            
            // vector from light position to intersection location
            Vector L = (lightLoc - intersectLoc).unit();
            
            Color lightColor = (*li)->getColor();
            
            
            
            float NdotL = max(dot(N, L), 0.0f);
            if (NdotL < 0)
                continue;
            diffuse += lightColor * NdotL;
            
            // specular
            float k = dot(N, (L + (r.origin-intersectLoc).unit()).unit());
            if (k > 0)  // for now arbitrary specular component
                specular += pow(k, 0.3) * lightColor;
        }
        // 'filter' the colors through each other
        result += ambient * diffuse * specular;
    }
    return result;
}

void Scene::render(const Camera &cam, int imageSize, ostream &os) const {
    os << "P3 " << imageSize << " " << imageSize << " " << 255 << endl;
    for (int y = 0; y < imageSize; y++)
    {
        for (int x = 0; x < imageSize; x++)
        {
            Ray pixelRay = cam.getRayForPixel(x, y, imageSize);
            Color pixelColor = raytrace(pixelRay);
            os << (int) (pixelColor.getRed() * 255) << " ";
            os << (int) (pixelColor.getGreen() * 255) << " ";
            os << (int) (pixelColor.getBlue() * 255) << " ";
            os << endl;
        }
    }
}