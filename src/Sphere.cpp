#include "Sphere.h"

int Sphere::getIntersections(const Ray &r, float &t1, float &t2) const {
    float a = dot(r.direction, r.direction);
    float b = 2 * (dot(r.origin, r.direction) - dot(r.direction, center));
    float c = dot(r.origin, r.origin) + dot(center, center) -
              2 * dot(r.origin, center) - radius * radius;
    float delta = b * b - 4 * a * c;
    if (delta < 0) {
        t1 = NO_INTERSECTION;
        t2 = NO_INTERSECTION;
        return 0;
    }
    t1 = (-b - sqrt(delta)) / (2 * a);
    t2 = (-b + sqrt(delta)) / (2 * a);
    
    if (t1 < 0) {
        if (t2 < 0) {
            t2 = NO_INTERSECTION;
        }
        t1 = t2;
    }
        
    if (delta == 0) {
        t2 = NO_INTERSECTION;
        return 1;
    }
    else
        return 2;
}

float Sphere::intersection(const Ray &r) const {
    float t1, t2;
    getIntersections(r, t1, t2);
    return t1;
}

const Vector Sphere::getNormalAtPoint(const Vector &point) const {
    return (point - center).unit();
    
}

const Color Sphere::getColorAtPoint(const Vector &point) const {
    return surfaceColor;
}