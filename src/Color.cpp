#include "Color.h"

bool Color::valid(float value) const {
    return (value >= 0) && (value <= 1);
}

inline float Color::clip(float value) const {
    if (value < 0)
        return 0;
    else if (value > 1)
        return 1;
    else
        return value;
}

Color::Color(float red, float green, float blue) {
    if (!(valid(red) && valid(green) && valid(blue)))
        throw(std::invalid_argument("Invalid color values."));
    else {
        r = red;
        g = green;
        b = blue;
    }    
}

Color::Color(const Color &c) {
    r = c.getRed();
    g = c.getGreen();
    b = c.getBlue();
}
    
const float Color::getRed() const {
    return r;
}
const float Color::getGreen() const {
    return g;
}
const float Color::getBlue() const {
    return b;
}
    
void Color::setRed(float value) {
    if (!valid(value))
        throw(std::invalid_argument("Value needs to be in 0-1 range."));
    r = value;
}

void Color::setGreen(float value) {
   if (!valid(value))
        throw(std::invalid_argument("Value needs to be in 0-1 range."));
    g = value;
}

void Color::setBlue(float value) {
   if (!valid(value))
        throw(std::invalid_argument("Value needs to be in 0-1 range."));
    b = value;
}

bool Color::operator==(const Color &c) const {
    return fabs(r - c.getRed()) < std::numeric_limits<float>::epsilon() &&
           fabs(g - c.getGreen()) < std::numeric_limits<float>::epsilon() &&
           fabs(b - c.getBlue()) < std::numeric_limits<float>::epsilon();
}

Color & Color::operator=(const Color &c) {
    if (this != &c) {
        r = c.getRed();
        b = c.getBlue();
        g = c.getGreen();
    }
    return *this;
}
    
Color & Color::operator+=(const Color &c) {
    if (this != &c) {
        r = clip(r + c.getRed());
        b = clip(b + c.getBlue());
        g = clip(g + c.getGreen());
    }
    return *this;
}

Color & Color::operator*=(const Color &c) {
    if (this != &c) {
        r = clip(r * c.getRed());
        b = clip(b * c.getBlue());
        g = clip(g * c.getGreen());
    }
    return *this;
}

Color & Color::operator-=(const Color &c) {
    if (this != &c) {
        r = clip(r - c.getRed());
        b = clip(b - c.getBlue());
        g = clip(g - c.getGreen());
    }
    return *this;
}

Color & Color::operator+=(float s) {
    r = clip(r + s);
    b = clip(b + s);
    g = clip(g + s);
    return *this;    
}

Color & Color::operator-=(float s) {
    r = clip(r - s);
    b = clip(b - s);
    g = clip(g - s);
    return *this;    
}
    
Color & Color::operator*=(float s) {
    r = clip(r * s);
    b = clip(b * s);
    g = clip(g * s);
    return *this;    
}

Color & Color::operator/=(float s) {
    if (s == 0.0f)
        throw(std::invalid_argument("Division by 0."));
        
    r = clip(r / s);
    b = clip(b / s);
    g = clip(g / s);
    
    return *this;    
}
        
const Color Color::operator+(const Color &c) const {
    Color result(*this);
    result += c;
    return result;
}
const Color Color::operator-(const Color &c) const {
    Color result(*this);
    result -= c;
    return result;
}
const Color Color::operator*(const Color &c) const {
    Color result(*this);
    result *= c;
    return result;
}
    
std::ostream & operator<<(std::ostream &os, const Color &c) {
    os << "(" << c.getRed() << ", " << c.getGreen() << ", " << c.getBlue() << ")" << std::endl;
    return os;
}

const Color operator+(const Color &c, float s) {
    Color result(c);
    result += s;
    return result;
}

const Color operator+(float s, const Color &c) {
    return c + s;
}

const Color operator*(const Color &c, float s) {
    Color result(c);
    result *= s;
    return result;
}
const Color operator*(float s, const Color &c) {
    return c * s;
}

const Color operator/(const Color &c, float s) {
    Color result(c);
    result /= s;
    return result;

}

