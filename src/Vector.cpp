#include "Vector.h"

/*
 * This file contains definitions for functions declared in Vector.h
 */

// Vector equality operator
bool Vector::operator==(const Vector &v) const {
    return (x == v.x) && (y == v.y) && (z == v.z);
}

// Vector inequality operator
bool Vector::operator!=(const Vector &v) const {
    return !(*this == v);
}

// Vector self-addition with a scalar
Vector & Vector::operator+=(float number) {
    x += number;
    y += number;
    z += number;
    return *this;
}

// Vector self-subtraction with a scalar
Vector & Vector::operator-=(float number) {
    *this += (-number);
    return *this;
}

// Vector self-multiplication with a scalar
Vector & Vector::operator*=(float number) {
    x *= number;
    y *= number;
    z *= number;
    return *this;
}

// Vector self-division by a scalar
Vector & Vector::operator/=(float number) {
    assert(number != 0);
    x /= number;
    y /= number;
    z /= number;
    return *this;
}

// Vector-scalar addition
const Vector operator+(const Vector &v, float s) {
    Vector result(v);
    result += s;
    return result;
}

// Scalar-vector addition
const Vector operator+(float s, const Vector &v) {
    return v + s;
}

// Vector-scalar subtraction
const Vector operator-(const Vector &v, float s) {
    Vector result(v);
    result -= s;
    return result;
}

// Scalar-vector subtraction
const Vector operator-(float s, const Vector &v) {
    Vector result(v);
    result -= s;
    result *= -1.0f;
    return result;
}

// Vector-scalar multiplication (right multiplication)
const Vector operator*(const Vector &v, float s) {
    Vector result(v);
    result *= s;
    return result;
}

// Scalar-vector multiplication (left multiplication)
const Vector operator*(float s, const Vector &v) {
    return v * s;
}

// Vector-scalar division (right division)
const Vector operator/(const Vector &v, float s) {
    if (s == 0.0f) {
        throw(std::invalid_argument("Division by 0."));
    }
    Vector result(v);
    result /= s;
    return result;
}

// Vector assignment
Vector & Vector::operator=(const Vector &v) {
    if (this != &v) {
        x = v.x;
        y = v.y;
        z = v.z;
    }
    return *this;
}

// Vector self-addition with a vector
Vector & Vector::operator+=(const Vector &v) {
    if (this != &v) {
        x += v.x;
        y += v.y;
        z += v.z;
    }
    return *this;
}

// Vector self-subtraction with a vector
Vector & Vector::operator-=(const Vector &v) {
    if (this != &v) {
        x -= v.x;
        y -= v.y;
        z -= v.z;
    }
    return *this;
}

// Vector norm
float Vector::norm() const{
    return sqrt(x * x + y * y + z * z);
}

// Vector-vector addition
const Vector operator+(const Vector &v, const Vector &s) {
    Vector result(v);
    result += s;
    return result;
}

// Vector-vector subtraction
const Vector operator-(const Vector &v, const Vector &u) {
    Vector result(v);
    result -= u;
    return result;
}

// Dot product
float dot(const Vector &u, const Vector &v){
    return u.x * v.x + u.y * v.y + u.z * v.z;
}

// Cross product
const Vector cross(const Vector &u, const Vector &v){
    Vector result;
    result.x = u.y * v.z - u.z * v.y;
    result.y = -(u.x * v.z - u.z * v.x);
    result.z = u.x * v.y - u.y * v.x;
    return result;
}

// Unit vector in the direction of the vector
Vector Vector::unit() const{
    // assert we're not a zero vector
    if (*this == Vector())
        throw(std::length_error("Attempting to get unit vector of a 0-norm."));
    Vector result(*this);
    result /= norm();
    return result;
}

// Vector projection of u onto v
const Vector project(const Vector &u, const Vector &v) {
    return (dot(u, v) / dot(v, v)) * v;
}

const Vector operator-(const Vector &v) {
    return -1.0 * v;
}

// Utility check whether a vector is a unit vector
bool Vector::isUnit() const {
    return norm() == 1.0f;
}

// Printing function
std::ostream& operator<<(std::ostream &os, const Vector &v) {
    os << "[" << v.x << " " << v.y << " " << v.z << "]" << std::endl;
    return os;
}