#include "Camera.h"

const Vector Camera::getDirection() const {
    return direction;
}
const Vector Camera::getLocation() const {
    return location;
}
const Vector Camera::getUp() const {
    return cameraUp;
}
const Vector Camera::getRight() const {
    return cameraRight;
}
const float Camera::getDistance() const {
    return distance;
}
const float Camera::getFieldOfView() const {
    return fov;
}

Camera::Camera(Vector position, Vector lookAt, Vector up, float f) {
    location = position;
    direction = (lookAt - location).unit();
    cameraRight = cross(direction, up).unit();
    cameraUp = cross(cameraRight, direction).unit();
    fov = f;
    distance = 0.5 / tan(DEGTORAD(fov / 2)); 
}

Ray Camera::getRayForPixel(int x, int y, int imgSize) const {
    Vector pixelDir = distance * direction +
                  (0.5 - (float) y / (float) (imgSize - 1)) * cameraUp +
                  ((float) x / (float) (imgSize - 1) - 0.5) * cameraRight;
                  
    Ray pixelRay(location, pixelDir, true);
    
    return pixelRay;
}