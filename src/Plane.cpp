#include "Plane.h"

float Plane::intersection(const Ray &r) const {
    float dn = dot(surfaceNormal, r.direction);
    if (dn == 0)
        return NO_INTERSECTION;
    float t = - (dot(r.origin, surfaceNormal) + distance) / dn;
    if (t < 0)
        return NO_INTERSECTION;
    return t;
}

const Vector Plane::getNormalAtPoint(const Vector &point) const {
    return surfaceNormal;
}

const Color Plane::getColorAtPoint(const Vector &point) const {
    return surfaceColor;
}